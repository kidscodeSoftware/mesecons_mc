-- WALL BUTTON
-- A button that when pressed emits power for 1 second
-- and then turns off again

mesecon.button_turnoff = function (pos)
	local node = minetest.get_node(pos)
	if node.name:find("mesecons_button:button_on") then --has not been dug
		if node.name:sub(-4) == "wood" then
			minetest.swap_node(pos, {name = "mesecons_button:button_off_wood", param2=node.param2})
		elseif node.name:sub(-5) == "stone" then
			minetest.swap_node(pos, {name = "mesecons_button:button_off_stone", param2=node.param2})
		end

		minetest.sound_play("mesecons_button_pop", {pos=pos})
		local rules = mesecon.rules.buttonlike_get(node)
		mesecon.receptor_off(pos, rules)
	end
end

local boxes_off = { -3/16, -2/16, 8/16, 3/16, 2/16, 6/16 }
local boxes_on = { -3/16, -2/16, 8/16, 3/16, 2/16, 7/16 }

local function register_button(material, sound)
	minetest.register_node("mesecons_button:button_off_"..material, {
		drawtype = "nodebox",
		tiles = {"default_"..material..".png"},
		paramtype = "light",
		paramtype2 = "facedir",
		legacy_wallmounted = true,
		walkable = false,
		on_rotate = mesecon.buttonlike_onrotate,
		sunlight_propagates = true,
		node_box = {
			type = "fixed",
			fixed = {boxes_off}
		},
		groups = {dig_immediate=2, mesecon_needs_receiver = 1, mesecons_content = 1},
		description = "Button",
		sounds = sound,
		on_rightclick = function (pos, node)
			minetest.swap_node(pos, {name = "mesecons_button:button_on_"..material, param2=node.param2})
			mesecon.receptor_on(pos, mesecon.rules.buttonlike_get(node))
			minetest.sound_play("mesecons_button_push", {pos=pos})
			minetest.after(1, mesecon.button_turnoff, pos)
		end,
		mesecons = {receptor = {
			state = mesecon.state.off,
			rules = mesecon.rules.buttonlike_get
		}}
	})

	minetest.register_node("mesecons_button:button_on_"..material, {
		drawtype = "nodebox",
		tiles = {"default_"..material..".png"},
		paramtype = "light",
		paramtype2 = "facedir",
		legacy_wallmounted = true,
		walkable = false,
		on_rotate = false,
		sunlight_propagates = true,
		node_box = {
			type = "fixed",
			fixed = {boxes_on}
		},
		groups = {dig_immediate=2, not_in_creative_inventory=1, mesecon_needs_receiver = 1, mesecons_content = 1},
		drop = 'mesecons_button:button_off_'..material,
		description = "Button",
		sounds = sound,
		mesecons = {receptor = {
			state = mesecon.state.on,
			rules = mesecon.rules.buttonlike_get
		}}
	})

	minetest.register_craft({
	output = "mesecons_button:button_off_"..material.." 2",
	recipe = {
		{"group:mesecon_conductor_craftable","group:"..material},
	}
})
end

register_button("wood", default.node_sound_wood_defaults())
register_button("stone", default.node_sound_stone_defaults())

